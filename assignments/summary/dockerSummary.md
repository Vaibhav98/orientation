# Docker
-----
1. Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.
2. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with  each other through well-defined channels.
3. All containers are run by a single operating system kernel and therefore use fewer resources than virtual machines.

#### Docker Commands

-----
Some Docker Commands are as following :
1. docker --version<br>
    This command is use to get currently installed version of Doker in your system.

2. Docker pull<br>
    usage: docker pull <br>
    This command is used to pull images from the docker repository(hub.docker.com).
![images](extras/img1.jpg)

3. docker run<br>
    Usage: docker run-it-d <br>
    This command is used to create a container from an image.<br>
    ![](extras/img3.jpg)

4. docker Ps <br>
    This command is used to list the running containers.<br>
    ![](extras/img2.jpg)

5. docker Ps -a <br>
    This command is used to show all the running and exited containers.<br>
    ![](extras/img2.jpg)

6. docker exec <br>
    usage: docker exec -it bash<br>
    This command is used to access the running container. <br>

7. docker stop <br>
    Usage: docker stop<br>
    This command stops a running container<br>

8. docker kill <br>
    Usage: docker kill<br>
    This command kills the container by stopping its execution immediately.<br>

9. docker commit <br>
    Usage: docker commit  <username/imagename><br>
    This command creates a new image of an edited container on the local system.<br>

10. docker login<br>
    Usage: docker login<br>
    This command is used to login to the docker hub repository<br>