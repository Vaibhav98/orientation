
# Git Summary

# What Is Git?

Git is a distributed version-control system for tracking changes in source code during software development. It makes collaboration with teammates supersimple and easy.

## Basic Overview of git main keywords

- **Repository:**<br>
A Repository is collection of files and folders (code files) that using git to track.<br>

-  **Commit:**<br>
Think of this as saving your work.The commit will only exist on your local machine until it is pushed to a remote repository.<br>

- **Push:**<br>
To synchronize your commited work with gitlab.<br>

- **Pull:**<br>
It pulls the changes to your local machine.<br>

- **Branch:**<br>
These are separate instances of the code that is different from the main codebase.<br>

- **Merge:**<br>
When a branch is free of bugs (as far as you can tell, at least), and ready to become part of the primary codebase,
it will get merged into the master branch.<br>

- **Clone:**<br>
It takes the entire online repository and makes an exact copy of it on your local machine.<br>

- **Fork:**<br>
Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine.
you get an entirely new repo of that code under your own name.<br>

## Git Workflow

* **Clone repo** : `$ git clone (link to repository)`

 * **Create new branch** : ` $ git checkout master` or `$ git checkout -b (your branch name)`

 * **Staging changes** : `$ git add `.

 * **Commit changes** : `$ git commit -sv`

 * **Push Files** : `$ git push origin (branch name)`

## Some Important Commands To Use Git

`git config `<br>
`git init ` <br>
`git clone `<br>
`git add `<br>
`git commit `<br>
`git diff `<br>
`git reset `<br>
`git status `<br>
`git rm `<br>
`git log `<br>
`git show `<br>
`git tag `<br>
`git branch `<br>
`git checkout `<br>
`git merge `<br>
`git remote `<br>
`git push `<br>
`git pull `<br>
`git stash `<br>

